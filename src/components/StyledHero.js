import styled from 'styled-components'
import defaultImg from "../images/hatchback/BMW1/bmw1.jpg";


const StyledHero = styled.header`
  min-height: 60vh; 
  background: url(${props => props.img? props.img:defaultImg}) center no-repeat;
  display: flex;
  align-items: center;
  justify-content: center;
`

export default StyledHero
import React from "react";
import {Link}  from 'react-router-dom'
import defaultImg from "../images/hatchback/BMW1/bmw1.jpg";
import img1 from "../images/hatchback/BMW6/bmw6.jpg";
import img3 from "../images/sedan/BMW3/bmw3.jpg";
import img4 from "../images/sedan/BMW4/bmw4.jpg";
import img5 from "../images/sedan/BMW5/bmw5.jpg";
import img6 from "../images/sedan/BMW6/bmw6.jpg";
import img7 from "../images/hatchback/BMW3/bmw3.jpg";
import img8 from "../images/hatchback/BMW4/bmw4.jpg";
import img9 from "../images/hatchback/BMW5/bmw5.jpg";
import img10 from "../images/sedan/BMW6/bmw6.jpg";
import img11 from "../images/hatchback/BMW1/bmw1.jpg";
import img12 from "../images/hatchback/BMW2/bmw2.jpg";

export default function MyCar({car}) {

    var img = document.createElement('img');


    console.log(car)
    const{idd,nameModel,mainPhoto,price} = car;

    function addcookie(){
        document.cookie = 'idd=' + idd
    }

    if(mainPhoto === "img1"){
        img.src = img1
        console.log(img.src)
    }
    else if(mainPhoto === "img3"){
        img.src = img1
        console.log(img.src)
    }
    else if(mainPhoto === "img4"){
        img.src = img4
        console.log(img.src)
    }
    else if(mainPhoto === "img5"){
        img.src = img5
        console.log(img.src)
    }
    else if(mainPhoto === "img6"){
        img.src = img6
        console.log(img.src)
    }
    else if(mainPhoto === "img7"){
        img.src = img7
        console.log(img.src)
    }
    else if(mainPhoto === "img8"){
        img.src = img8
        console.log(img.src)
    }
    else if(mainPhoto === "img9"){
        img.src = img9
        console.log(img.src)
    }
    else if(mainPhoto === "img10"){
        img.src = img10
        console.log(img.src)
    }
    else if(mainPhoto === "img11"){
        img.src = img11
        console.log(img.src)
    }
    else if(mainPhoto === "img12"){
        img.src = img12
        console.log(img.src)
    }

    return (

        <article onClick={addcookie} className="car">
            <div className="img-container">

                <div className="price-top">
                    <p>starting from</p>
                    <h6>${price}</h6>
                </div>

                <img src={img.src} alt="single car"/>
                <Link  to={`/Cars/${nameModel}`} className="btn-primary car-link">
                    Features
                </Link>
            </div>
            <p className="car-info">{nameModel}
            </p>
        </article>
    );
}


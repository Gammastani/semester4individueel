import React, {Component} from "react";
import { Link } from 'react-router-dom';
import axios from "axios";
import MyCar from "./MyCar";

function GetCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
}


export default class MyCarsContainer extends Component {
    state = {
        cars: [],
    };

    componentDidMount() {
        axios.get(`http://localhost:8080/usercars?id=${GetCookie('id')}`)
            .then(res => {
                const cars = res.data;
                this.setState({ cars });
            })
    }



    render() {
        return (
            <>
                <div className="mycars">
                    <h2>My Cars</h2>
                </div>
            <section className="carslist">
                <div className="carslist-center">
                    {this.state.cars.map(car => {
                        return <MyCar key={car.id} car={car}/>
                    })}
                </div>
            </section>
                </>
        );
    }

}


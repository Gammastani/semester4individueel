import React, {Component} from 'react'
import Title from './Title'
import {FaUserCircle, FaPhone, FaEnvelope, MdPlace, FaCalendarAlt, FaBeer} from "react-icons/all";

export default class Services extends Component {
    state = {
        services: [
            {
                icon: <FaUserCircle/>,
                icon2: <FaPhone/>,
                icon3: <FaEnvelope/>,
                title: "Contact",
                info:  '+31 647688942',
                info2: 'alpteking_55@hotmail.com'
            },
            {
                icon: <MdPlace/>,
                title: "Our Location",
                info: 'Ringbaan Oost 102a',
                info2: '5013 CD Tilburg'
            },
            {
                icon: <FaCalendarAlt/>,
                title: "Opening Hours",
                info: 'Monday: 08:00 - 21:00',
                info2:'Tuesday: 08:00 - 21:00',
                info3:'Wednesday: 12:00 - 20:00',
                info4:'Thursday: 08:00 - 21:00',
                info5:'Friday: 12:00 - 20:00',
                info6:'Saturday: 12:00 - 18:00',
                info7:'Sunday: Closed'
            }
        ]
    };

    render() {
        return (
            <section className="services">
                <Title title='services'/>
                <div className={"services-center"}>
                {this.state.services.map((item, index) => {
                    return (
                        <article key={index} className="service">
                            <span>{item.icon}</span>
                            <h6>{item.title}</h6>
                            <p>{item.icon2}{item.info}<br/>{item.icon3} {item.info2}<br/>{item.info3}<br/>{item.info4}<br/>{item.info5}<br/>{item.info6}<br/>{item.info7}</p>
                        </article>
                    );
                })}
                </div>
            </section>
        )
    }
}
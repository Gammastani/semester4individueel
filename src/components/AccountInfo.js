import React, {Component}from 'react';
import {Link} from "react-router-dom";


function GetCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
}

function ClearCookies() {
    document.cookie.split(";").forEach(function (c) {
        document.cookie = c.replace(/^ +/, "").replace(/=.*/, "=;expires=" + new Date().toUTCString() + ";path=/");
    });
    window.location.href = 'http://localhost:3000/';
}


export default class AccountInfo extends Component {
    state = {
        services: [
            {
                email: null,
                name: null,
                lastName: null
            },
        ]
    };


    render() {

        return (
            <>
                <div className="ProfileContent">
                    <div>Name: {GetCookie('name')}</div>
                    <div>Last name: {GetCookie('lastname')}</div>
                    <div>Email: {GetCookie('email')}</div>
                    <Link id="logout" onClick={ClearCookies} to="/" href="#" className="lout">Logout</Link>
                </div>
            </>

        )
    }
}
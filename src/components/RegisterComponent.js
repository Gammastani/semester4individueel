import React from "react";


const RegisterComponent = (props) => {
    return (
        <div className="login-page">
            <div className="form" onSubmit={props.getUser}>
                <form className="login-form">
                    <input required={true} type="text" name="email"  placeholder="Email address"/>
                    <input required={true} type="password" name="password" placeholder="Password"/>
                    <input required={true} type="text" name="name" placeholder="Name"/>
                    <input required={true} type="text" name="lastname" placeholder="Last name"/>
                    <button>create</button>
                    <p className="message">Already registered? <a href="/login">Back to login</a></p>
                </form>
            </div>
        </div>
    );
};

export default RegisterComponent;

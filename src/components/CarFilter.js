import React from 'react';
import {useContext} from 'react'
import {CarContext} from "../Context";
import Title from "./Title";

//get all unique values
const getUnique = (items,value)=>{
    return[...new Set(items.map(item => item[value]))]
}

export default function CarFilter({cars}) {

    const context = useContext(CarContext)
    const{
        handleChange,type,seats,price,minPrice,maxPrice,minSize,maxSize,automatic,diesel
    } = context;

    // get unique types
let types = getUnique(cars,'type');
    // add all
    types = ['all',...types];
    //map to jsx
    types = types.map((item,index)=>{
        return <option value={item} key={index}>{item}</option>
    })
    let people = getUnique(cars,'seats');
    people = people.map((item,index)=>{
        return <option key={index} value={item}>{item}</option>
    })

    return (
        <section className="filter-container">
            <Title title="search cars"/>
            <form className="filter-form">
                {/*select type*/}
                <div className="form-group">
                    <label htmlFor="type">car type</label>
                    <select
                        name="type"
                        id="type"
                        value={type}
                        className="form-control"
                        onChange={handleChange}>
                        {types}
                    </select>
                </div>
                {/*end select type*/}
                {/*select type*/}
                <div className="form-group">
                    <label htmlFor="seats">seats</label>
                    <select
                        name="seats"
                        id="seats"
                        value={seats}
                        className="form-control"
                        onChange={handleChange}>
                        {people}
                    </select>
                </div>
                {/*end select type*/}
                {/*Car price */}
                <div className="form-group">
                    <label htmlFor="form-group">
                        Car price €{price}
                    </label>
                    <input type="range" name="price" min={minPrice} max={maxPrice} id="price"
                           value={price} onChange={handleChange} className="form-control"/>
                </div>
                {/*end Car price */}
                {/*clutch */}
                     <div className="form-group">
                        <div className="single-extra">
                            <input
                                type="checkbox"
                                name="automatic"
                                id="automatic"
                                checked={automatic}
                                onChange={handleChange}/>
                            <label htmlFor="automatic">automatic</label>
                        </div>
                         {/*fuel */}
                         <div className="single-extra">
                             <input
                                 type="checkbox"
                                 name="diesel"
                                 id="diesel"
                                 checked={diesel}
                                 onChange={handleChange}/>
                             <label htmlFor="diesel">Diesel</label>
                         </div>
                         {/*end fuel */}
                     </div>
                {/*end clutch */}


            </form>
        </section>
    );
};


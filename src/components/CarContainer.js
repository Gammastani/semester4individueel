import React from 'react';
import CarFilter from './CarFilter'
import CarList from './CarList'
import {withCarConsumer} from "../Context";
import Loading from './Loading'

function CarContainer({context}) {

    const {loading, sortedCars, cars} = context;
    if (loading) {
        return <Loading/>
    }
    return (
        <>
            <CarFilter cars={cars}/>
            <CarList cars={sortedCars}/>
        </>
    )
}


export default withCarConsumer(CarContainer)

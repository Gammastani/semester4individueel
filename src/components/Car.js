import React from "react";
import {Link}  from 'react-router-dom'
import defaultImg from "../images/hatchback/BMW1/bmw1.jpg";
import PropTypes from "prop-types";


export default function Car({car}) {


    const{idd,name,slug,images,price} = car;

    function addcookie(){
        document.cookie = 'idd=' + idd
    }

        return (

            <article onClick={addcookie} className="car">
                <div className="img-container">
                    <img src={images[0] || defaultImg} alt="single car"/>
                    <div className="price-top">
                        <p>starting from</p>
                        <h6>${price}</h6>
                    </div>
                    <Link  to={`/Cars/${slug}`} className="btn-primary car-link">
                        Features
                    </Link>
                </div>
                <p className="car-info">{name}
                </p>
            </article>
        );
    }

    Car.propTypes = {
    car:PropTypes.shape({
        name:PropTypes.string.isRequired,
        slug:PropTypes.string.isRequired,
        images:PropTypes.arrayOf(PropTypes.string).isRequired,
        price:PropTypes.number.isRequired,
    })
}
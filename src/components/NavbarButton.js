import React from 'react';
import {Link} from "react-router-dom";

function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
}


export default function NavbarButton(){
    if (document.cookie.indexOf('id') != -1) {

        return (
            <>
                <li>
                    <Link to="/MyCars">MyCars</Link>
                </li>
            <li className="right">
                <Link id="profile" to="/Profile">{getCookie('name')}</Link>
            </li>
                </>
        )
    }
        return (
            <li className="right">
                <Link to="/login">Account</Link>
            </li>
        )

}
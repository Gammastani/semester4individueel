import React from "react";
import { Link } from 'react-router-dom';

const Logincomponent = (props) => {
    return (
        <div className="login-page">
            <div className="form">
                <form className="login-form" onSubmit={props.getUser}>
                    <input required={true} type="email" name="email" placeholder="Email" />
                    <input required={true} type="password" name="password" placeholder="password"/>
                    <button id='login'>login</button>
                    <br/>
                    <br/>
                    <Link className="message" to="/Register">Not registered?</Link>
                </form>
            </div>
        </div>
    );
};

export default Logincomponent;
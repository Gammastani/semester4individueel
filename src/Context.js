import React, {Component} from 'react';
import items from "./data"

const CarContext = React.createContext();

// <CarContext.Provider value = {}


class CarProvider extends Component {
    state = {
        cars: [],
        sortedCars: [],
        featuredCars: [],
        loading: true,
        type: 'all',
        seats: 1,
        price: 0,
        minPrice: 0,
        maxPrice: 0,
        minSeats: 0,
        maxSeats: 0,
        automatic: false,
        diesel: false
    }

    //getData


    componentDidMount() {
        // this.getData
        let cars = this.formatData(items)
        let featuredCars = cars.filter(car => car.featured === true);
        let maxPrice = Math.max(...cars.map(item => item.price))
        let maxSeats = Math.max(...cars.map(item => item.seats))
        this.setState({
            cars,
            featuredCars,
            sortedCars: cars,
            loading: false,
            price: maxPrice,
            maxPrice,
            maxSeats
        })
    }

    formatData(items) {
        let tempItems = items.map(item => {
            let id = item.sys.id
            let images = item.fields.images.map(image => image.fields.file.url)
            let car = {...item.fields, images, id};
            return car;
        })
        return tempItems
    }

    getCar = (slug) => {
        let tempCars = [...this.state.cars];
        const car = tempCars.find((car) => car.slug === slug);
        return car;
    }

    handleChange = event => {
        const target = event.target
        const value = target.type === "checkbox" ? target.checked:target.value
        const name = event.target.name
        this.setState({
            [name]:value
        },this.filterCars)

    }

    filterCars = () => {
        let{
            cars,type,seats,price,minSeats,maxSeats,automatic,diesel

        } = this.state


        let tempCars = [...cars]
        seats = parseInt(seats)
        price = parseInt(price)

    //filter by type
        if(type !== 'all'){
            tempCars = tempCars.filter(car => car.type === type)
        }
        //filter by seats
        if(seats !== 1){
            tempCars = tempCars.filter(car => car.seats >= seats)
        }
        //filter price
        tempCars = tempCars.filter(car => car.price <= price)

        //filter price
        if(automatic){
            tempCars = tempCars.filter(car => car.automatic === true)
        }
        if(diesel){
            tempCars = tempCars.filter(car => car.diesel === true)
        }


        this.setState({
            sortedCars:tempCars
        })
    }

    render() {
        return <CarContext.Provider
            value={{
                ...this.state,
                getCar: this.getCar,
                handleChange: this.handleChange
            }}>
            {this.props.children}
        </CarContext.Provider>
    }
}

const CarConsumer = CarContext.Consumer;

export function withCarConsumer(Component) {
    return function ConsumerWrapper(props) {
        return <CarConsumer>
            {
                value => <Component {...props} context={value}/>
            }
        </CarConsumer>
    }

}

export {CarProvider, CarConsumer, CarContext}
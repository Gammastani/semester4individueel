import React, {Component} from "react";
import axios from "axios";
import {CarContext} from "../Context";
import defaultBcg from "../images/hatchback/BMW1/bmw1.jpg";



function GetCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
}


class LoginPage extends Component {

    constructor(props) {
        super(props)
        //  console.log(this.props)
        this.state ={
            slug:this.props.match.params.slug,
            defaultBcg
        }
    }


    static contextType = CarContext;

    BuyCar = (e) => {
        e.preventDefault();
        const {getCar} = this.context;
        const carId = getCar(this.state.slug).id;
        const userId = GetCookie('id')

        if (carId && userId) {
            const params = new URLSearchParams();
            params.append('userId', userId);
            params.append('carId', carId);
            axios({
                method: 'post',
                url: 'http://localhost:8080/BuyCar',
                data: params
            }).then((res) => {window.location.href = 'http://localhost:3000/';})
        }
    };


    render() {
        return (
            <div>
              {this.BuyCar}
            </div>
        );
    }
}
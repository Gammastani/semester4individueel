import React from "react";
import Hero from '../components/Hero'
import {Link} from 'react-router-dom'
import Banner from '../components/Banner'
import CarContainer from "../components/CarContainer";


const Cars = () => {
    return (
        <>
        <Hero hero="carsHero">
            <Banner title='our Cars'>
                <Link to='/' className='btn-primary'>
                    return home
                </Link>
            </Banner>
        </Hero>
            <CarContainer/>
            </>
    )
};

export default Cars;
import React, {Component} from 'react';
import RegisterComponent from "../components/RegisterComponent";
import axios from "axios";

class LoginPage extends Component {
    state = {
        email: null,
        name: null,
        lastname: null,
        password: null
    };

    RegisterUser = (e) => {
        e.preventDefault();
        const email = e.target.elements.email.value;
        const name = e.target.elements.name.value;
        const lastname = e.target.elements.lastname.value;
        const password = e.target.elements.password.value;
        if (email && name && lastname && password) {
            const params = new URLSearchParams();
            params.append('email', email);
            params.append('name', name);
            params.append('lastname', lastname);
            params.append('password', password);
            axios({
                method: 'post',
                url: 'http://localhost:8080/register',
                data: params
            }).then((res) => {window.location.href = 'http://localhost:3000/Login';})
        }
    };
    render() {
        return (
            <div>
                <RegisterComponent getUser={this.RegisterUser}/>
            </div>
        );
    }
}

export default LoginPage;

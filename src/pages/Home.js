import React from "react";
import Hero from '../components/Hero'
import Banner from '../components/Banner'
import {Link} from 'react-router-dom'
import Services from '../components/Services'
import FeaturedCars from "../components/FeaturedCars";


export default function Home() {
    return (
        <>
            <Hero>
                <Banner title="luxurious cars" subtitle="deluxe cars starting from $14.999">
                    <Link to='/Cars' className="btn-primary">
                        our cars
                    </Link>
                </Banner>
            </Hero>
            <FeaturedCars/>
            <Services/>
        </>
    );
}
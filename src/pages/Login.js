import React, {Component} from 'react';
import LoginComponent from "../components/LoginComponent";
import axios from "axios"

class Login extends Component {
    state = {
        id: null,
        email: null,
        password: null,
        name: null,
        lastName: null
    };

    getUser = (e) => {
        e.preventDefault();
        const em = e.target.elements.email.value;
        const pw = e.target.elements.password.value;
        if (em && pw){
            axios.get(`http://localhost:8080/login?em=${em}&pw=${pw}`)
                .then((res) => {
                    const id = res.data.id;
                    const name = res.data.name;
                    const email = res.data.email;
                    const lastname = res.data.lastName;
                    document.cookie = 'id=' + id;
                    document.cookie = 'email=' + email;
                    document.cookie = 'name=' + name;
                    document.cookie = 'lastname=' + lastname;
                    window.location.href = 'http://localhost:3000/';
                } );
        }
    };

    render() {
        return (
            <div>
                <LoginComponent getUser={this.getUser}/>
            </div>
        );
    }
}

export default Login;

import React, {Component} from "react";
import defaultBcg from "../images/hatchback/BMW1/bmw1.jpg";
import Banner from '../components/Banner'
import {Link} from 'react-router-dom'
import {CarContext} from "../Context";
import StyledHero from "../components/StyledHero";
import axios from "axios";

function GetCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
}


export default class SingleCar extends
    Component{
    static contextType = CarContext;
    constructor(props) {
        super(props)
      //  console.log(this.props)
        this.state ={
            slug:this.props.match.params.slug,
            defaultBcg
        }
    }

    BuyCar = (e) => {
        e.preventDefault();
        const carId = GetCookie('idd')
        const userId = GetCookie('id')
        if (carId && userId) {
            const params = new URLSearchParams();
            params.append('userId', parseInt(userId));
            params.append('carId', parseInt(carId));
            axios({
                method: 'post',
                url: 'http://localhost:8080/BuyCar',
                data: params
            }).then((res) => {window.location.href = 'http://localhost:3000/';})
        }
    };







    render() {
        const {getCar} = this.context;
        const car = getCar(this.state.slug);



        if(!car) {
            return (
                <div className="error">
                <h3>no such car could be found...</h3>
                <Link to="/cars" className="btn-primary">
                    back to cars
                </Link>
            </div>
            )
        }
        const {idd, name,description,doors, seats,price,extras,automatic,diesel,images} = car
        const [mainImg,...defaultImg] = images;
        let Automatic = "manual";
            if(automatic){
                Automatic = "automatic";
            }
        let Diesel = "Benzine";
        if(diesel){
            Diesel = "Diesel";
        }


        return(

            <>
        <StyledHero img={mainImg || this.state.default}>
            <Banner style='max-width: 400px' title={`${name}`}>
                <Link to={'/Cars'} className='btn-primary'>
                    back to cars
                </Link>
            </Banner>
         </StyledHero>
         <section className="single-car">
        <div className="single-car-images">
            {defaultImg.map((item,index) =>{
             return   <img key={index} src={item} alt={name}/>
            })}
        </div>
             <div className="single-car-info">
                 <article className="description">
                     <h3>details</h3>
                     <p>{description}</p>
                 </article>
                 <article className="info">
                 <h3>info</h3>
                 <h6>price : €{price}</h6>
                 <h6>seats : {seats}</h6>
                 <h6>doors : {doors}</h6>
                 <h6>clutch : {Automatic}</h6>
                 <h6>fuel : {Diesel}</h6>

             </article>
                 <article >
                     <button onClick={this.BuyCar} to="/Cars" href="#" className="buy">Buy car</button>
                 </article>
             </div>

         </section>
                <section className="car-extras">
                    <h6>extras</h6>
                    <ul className="extras">
                        {extras.map((item,index)=>{
                            return <li key={index}>-{item}</li>
                        })}
                    </ul>
                </section>

            </>
        )
    }
}
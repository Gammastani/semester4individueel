
import img1 from "./images/hatchback/BMW6/bmw6.jpg";
import img3 from "./images/sedan/BMW3/bmw3.jpg";
import img4 from "./images/sedan/BMW4/bmw4.jpg";
import img5 from "./images/sedan/BMW5/bmw5.jpg";
import img6 from "./images/sedan/BMW6/bmw6.jpg";
import img7 from "./images/hatchback/BMW3/bmw3.jpg";
import img8 from "./images/hatchback/BMW4/bmw4.jpg";
import img9 from "./images/hatchback/BMW5/bmw5.jpg";
import img10 from "./images/sedan/BMW6/bmw6.jpg";
import img11 from "./images/hatchback/BMW1/bmw1.jpg";
import img12 from "./images/hatchback/BMW2/bmw2.jpg";

export default [
  {
    sys: {
      id: "1"
    },
    fields: {
      idd:"1",
      name: "1-serie Hatchback",
      slug: "1-serie Hatchback",
      type: "sedan",
      price: 15000,
      size: 4,
      seats: 2,
      diesel: false,
      automatic: false,
      featured: false,
      description:
        "Street art edison bulb gluten-free, tofu try-hard lumbersexual brooklyn tattooed pickled chambray. Actually humblebrag next level, deep v art party wolf tofu direct trade readymade sustainable hell of banjo. Organic authentic subway tile cliche palo santo, street art XOXO dreamcatcher retro sriracha portland air plant kitsch stumptown. Austin small batch squid gastropub. Pabst pug tumblr gochujang offal retro cloud bread bushwick semiotics before they sold out sartorial literally mlkshk. Vaporware hashtag vice, sartorial before they sold out pok pok health goth trust fund cray.",
      extras: [
        "Four-wheel drive",
        "Wireless CarPlay/Android Auto",
        "Interior Assistant",
        "Adaptive LED headlights",
        "Heated steering wheel",
        "Adaptive cruise control with traffic assist",
        "BMW's 360-degree camera and remote parking technology"
      ],
      images: [
        {
          fields: {
            file: {
              url: img1
            }
          }
        },
        {
          fields: {
            file: {
              url: img1
            }
          }
        },
        {
          fields: {
            file: {
              url: img1
            }
          }
        },
        {
          fields: {
            file: {
              url: img1
            }
          }
        }
      ]
    }
  },
  {
    sys: {
      id: "3"
    },
    fields: {
      idd:"3",
      name: "1-serie Coupé",
      slug: "1-serie Coupé",
      type: "sedan",
      price: 30000,
      size: 300,
      seats: 5,
      diesel: true,
      automatic: false,
      featured: false,
      description:
        "Street art edison bulb gluten-free, tofu try-hard lumbersexual brooklyn tattooed pickled chambray. Actually humblebrag next level, deep v art party wolf tofu direct trade readymade sustainable hell of banjo. Organic authentic subway tile cliche palo santo, street art XOXO dreamcatcher retro sriracha portland air plant kitsch stumptown. Austin small batch squid gastropub. Pabst pug tumblr gochujang offal retro cloud bread bushwick semiotics before they sold out sartorial literally mlkshk. Vaporware hashtag vice, sartorial before they sold out pok pok health goth trust fund cray.",
      extras: [
        "Four-wheel drive",
        "Wireless CarPlay/Android Auto",
        "Interior Assistant",
        "Adaptive LED headlights",
        "Heated steering wheel",
        "Adaptive cruise control with traffic assist",
        "BMW's 360-degree camera and remote parking technology"
      ],
      images: [
        {
          fields: {
            file: {
              url: img3
            }
          }
        },
        {
          fields: {
            file: {
              url: img3
            }
          }
        },
        {
          fields: {
            file: {
              url: img3
            }
          }
        },
        {
          fields: {
            file: {
              url: img3
            }
          }
        }
      ]
    }
  },
  {
    sys: {
      id: "4"
    },
    fields: {
      idd:"4",
      name: "1-serie Cabrio",
      slug: "1-serie Cabrio",
      type: "sedan",
      price: 20000,
      size: 400,
      seats: 5,
      diesel: true,
      automatic: true,
      featured: false,
      description:
        "Street art edison bulb gluten-free, tofu try-hard lumbersexual brooklyn tattooed pickled chambray. Actually humblebrag next level, deep v art party wolf tofu direct trade readymade sustainable hell of banjo. Organic authentic subway tile cliche palo santo, street art XOXO dreamcatcher retro sriracha portland air plant kitsch stumptown. Austin small batch squid gastropub. Pabst pug tumblr gochujang offal retro cloud bread bushwick semiotics before they sold out sartorial literally mlkshk. Vaporware hashtag vice, sartorial before they sold out pok pok health goth trust fund cray.",
      extras: [
        "Four-wheel drive",
        "Wireless CarPlay/Android Auto",
        "Interior Assistant",
        "Adaptive LED headlights",
        "Heated steering wheel",
        "Adaptive cruise control with traffic assist",
        "BMW's 360-degree camera and remote parking technology"
      ],
      images: [
        {
          fields: {
            file: {
              url: img4
            }
          }
        },
        {
          fields: {
            file: {
              url: img4
            }
          }
        },
        {
          fields: {
            file: {
              url: img4
            }
          }
        },
        {
          fields: {
            file: {
              url: img4
            }
          }
        }
      ]
    }
  },
  {
    sys: {
      id: "5"
    },
    fields: {
      idd:"5",
      name: "2-serie Coupé",
      slug: "2-serie Coupé",
      type: "sedan",
      price: 25000,
      doors: 4,
      seats: 5,
      diesel: false,
      automatic: false,
      featured: false,
      description:
        "Street art edison bulb gluten-free, tofu try-hard lumbersexual brooklyn tattooed pickled chambray. Actually humblebrag next level, deep v art party wolf tofu direct trade readymade sustainable hell of banjo. Organic authentic subway tile cliche palo santo, street art XOXO dreamcatcher retro sriracha portland air plant kitsch stumptown. Austin small batch squid gastropub. Pabst pug tumblr gochujang offal retro cloud bread bushwick semiotics before they sold out sartorial literally mlkshk. Vaporware hashtag vice, sartorial before they sold out pok pok health goth trust fund cray.",
      extras: [
        "Four-wheel drive",
        "Wireless CarPlay/Android Auto",
        "Interior Assistant",
        "Adaptive LED headlights",
        "Heated steering wheel",
        "Adaptive cruise control with traffic assist",
        "BMW's 360-degree camera and remote parking technology"
      ],
      images: [
        {
          fields: {
            file: {
              url: img5
            }
          }
        },
        {
          fields: {
            file: {
              url: img5
            }
          }
        },
        {
          fields: {
            file: {
              url: img5
            }
          }
        },
        {
          fields: {
            file: {
              url: img5
            }
          }
        }
      ]
    }
  },
  {
    sys: {
      id: "6"
    },
    fields: {
      idd:"6",
      name: "3-serie Sedan",
      slug: "3-serie Sedan",
      type: "sedan",
      price: 30000,
      doors: 4,
      seats: 5,
      diesel: false,
      automatic: false,
      featured: false,
      description:
        "Street art edison bulb gluten-free, tofu try-hard lumbersexual brooklyn tattooed pickled chambray. Actually humblebrag next level, deep v art party wolf tofu direct trade readymade sustainable hell of banjo. Organic authentic subway tile cliche palo santo, street art XOXO dreamcatcher retro sriracha portland air plant kitsch stumptown. Austin small batch squid gastropub. Pabst pug tumblr gochujang offal retro cloud bread bushwick semiotics before they sold out sartorial literally mlkshk. Vaporware hashtag vice, sartorial before they sold out pok pok health goth trust fund cray.",
      extras: [
        "Four-wheel drive",
        "Wireless CarPlay/Android Auto",
        "Interior Assistant",
        "Adaptive LED headlights",
        "Heated steering wheel",
        "Adaptive cruise control with traffic assist",
        "BMW's 360-degree camera and remote parking technology"
      ],
      images: [
        {
          fields: {
            file: {
              url: img6
            }
          }
        },
        {
          fields: {
            file: {
              url: img6
            }
          }
        },
        {
          fields: {
            file: {
              url: img6
            }
          }
        },
        {
          fields: {
            file: {
              url: img6
            }
          }
        }
      ]
    }
  },
  {
    sys: {
      id: "7"
    },
    fields: {
      idd:"7",
      name: "3-serie Touring",
      slug: "3-serie Touring",
      type: "sedan",
      price: 40000,
      doors: 4,
      seats: 5,
      diesel: true,
      automatic: false,
      featured: false,
      description:
        "Street art edison bulb gluten-free, tofu try-hard lumbersexual brooklyn tattooed pickled chambray. Actually humblebrag next level, deep v art party wolf tofu direct trade readymade sustainable hell of banjo. Organic authentic subway tile cliche palo santo, street art XOXO dreamcatcher retro sriracha portland air plant kitsch stumptown. Austin small batch squid gastropub. Pabst pug tumblr gochujang offal retro cloud bread bushwick semiotics before they sold out sartorial literally mlkshk. Vaporware hashtag vice, sartorial before they sold out pok pok health goth trust fund cray.",
      extras: [
        "Four-wheel drive",
        "Wireless CarPlay/Android Auto",
        "Interior Assistant",
        "Adaptive LED headlights",
        "Heated steering wheel",
        "Adaptive cruise control with traffic assist",
        "BMW's 360-degree camera and remote parking technology"
      ],
      images: [
        {
          fields: {
            file: {
              url: img7
            }
          }
        },
        {
          fields: {
            file: {
              url: img7
            }
          }
        },
        {
          fields: {
            file: {
              url: img7
            }
          }
        },
        {
          fields: {
            file: {
              url: img7
            }
          }
        }
      ]
    }
  },
  {
    sys: {
      id: "8"
    },
    fields: {
      idd:"8",
      name: "M3 Coupé",
      slug: "M3 Coupé",
      type: "hatchback",
      price: 30000,
      doors: 4,
      seats: 5,
      diesel: true,
      automatic: true,
      featured: true,
      description:
        "Street art edison bulb gluten-free, tofu try-hard lumbersexual brooklyn tattooed pickled chambray. Actually humblebrag next level, deep v art party wolf tofu direct trade readymade sustainable hell of banjo. Organic authentic subway tile cliche palo santo, street art XOXO dreamcatcher retro sriracha portland air plant kitsch stumptown. Austin small batch squid gastropub. Pabst pug tumblr gochujang offal retro cloud bread bushwick semiotics before they sold out sartorial literally mlkshk. Vaporware hashtag vice, sartorial before they sold out pok pok health goth trust fund cray.",
      extras: [
        "Four-wheel drive",
        "Wireless CarPlay/Android Auto",
        "Interior Assistant",
        "Adaptive LED headlights",
        "Heated steering wheel",
        "Adaptive cruise control with traffic assist",
        "BMW's 360-degree camera and remote parking technology"
      ],
      images: [
        {
          fields: {
            file: {
              url: img8
            }
          }
        },
        {
          fields: {
            file: {
              url: img8
            }
          }
        },
        {
          fields: {
            file: {
              url: img8
            }
          }
        },
        {
          fields: {
            file: {
              url: img8
            }
          }
        }
      ]
    }
  },
  {
    sys: {
      id: "9"
    },
    fields: {
      idd:"9",
      name: "M3 Cabriolet",
      slug: "M3 Cabriolet",
      type: "hatchback",
      price: 35000,
      doors: 4,
      seats: 5,
      diesel: false,
      automatic: false,
      featured: false,
      description:
        "Street art edison bulb gluten-free, tofu try-hard lumbersexual brooklyn tattooed pickled chambray. Actually humblebrag next level, deep v art party wolf tofu direct trade readymade sustainable hell of banjo. Organic authentic subway tile cliche palo santo, street art XOXO dreamcatcher retro sriracha portland air plant kitsch stumptown. Austin small batch squid gastropub. Pabst pug tumblr gochujang offal retro cloud bread bushwick semiotics before they sold out sartorial literally mlkshk. Vaporware hashtag vice, sartorial before they sold out pok pok health goth trust fund cray.",
      extras: [
        "Four-wheel drive",
        "Wireless CarPlay/Android Auto",
        "Interior Assistant",
        "Adaptive LED headlights",
        "Heated steering wheel",
        "Adaptive cruise control with traffic assist",
        "BMW's 360-degree camera and remote parking technology"
      ],
      images: [
        {
          fields: {
            file: {
              url: img9
            }
          }
        },
        {
          fields: {
            file: {
              url: img9
            }
          }
        },
        {
          fields: {
            file: {
              url: img9
            }
          }
        },
        {
          fields: {
            file: {
              url: img9
            }
          }
        }
      ]
    }
  },
  {
    sys: {
      id: "10"
    },
    fields: {
      idd:"10",
      name: "5-serie Sedan",
      slug: "5-serie Sedan",
      type: "hatchback",
      price: 40000,
      doors: 4,
      seats: 5,
      diesel: false,
      automatic: false,
      featured: false,
      description:
        "Street art edison bulb gluten-free, tofu try-hard lumbersexual brooklyn tattooed pickled chambray. Actually humblebrag next level, deep v art party wolf tofu direct trade readymade sustainable hell of banjo. Organic authentic subway tile cliche palo santo, street art XOXO dreamcatcher retro sriracha portland air plant kitsch stumptown. Austin small batch squid gastropub. Pabst pug tumblr gochujang offal retro cloud bread bushwick semiotics before they sold out sartorial literally mlkshk. Vaporware hashtag vice, sartorial before they sold out pok pok health goth trust fund cray.",
      extras: [
        "Four-wheel drive",
        "Wireless CarPlay/Android Auto",
        "Interior Assistant",
        "Adaptive LED headlights",
        "Heated steering wheel",
        "Adaptive cruise control with traffic assist",
        "BMW's 360-degree camera and remote parking technology"
      ],
      images: [
        {
          fields: {
            file: {
              url: img10
            }
          }
        },
        {
          fields: {
            file: {
              url: img10
            }
          }
        },
        {
          fields: {
            file: {
              url: img10
            }
          }
        },
        {
          fields: {
            file: {
              url: img10
            }
          }
        }
      ]
    }
  },
  {
    sys: {
      id: "11"
    },
    fields: {
      idd:"11",
      name: "5-serie Touring",
      slug: "5-serie Touring",
      type: "hatchback",
      price: 50000,
      doors: 4,
      seats: 5,
      diesel: true,
      automatic: false,
      featured: false,
      description:
        "Street art edison bulb gluten-free, tofu try-hard lumbersexual brooklyn tattooed pickled chambray. Actually humblebrag next level, deep v art party wolf tofu direct trade readymade sustainable hell of banjo. Organic authentic subway tile cliche palo santo, street art XOXO dreamcatcher retro sriracha portland air plant kitsch stumptown. Austin small batch squid gastropub. Pabst pug tumblr gochujang offal retro cloud bread bushwick semiotics before they sold out sartorial literally mlkshk. Vaporware hashtag vice, sartorial before they sold out pok pok health goth trust fund cray.",
      extras: [
        "Four-wheel drive",
        "Wireless CarPlay/Android Auto",
        "Interior Assistant",
        "Adaptive LED headlights",
        "Heated steering wheel",
        "Adaptive cruise control with traffic assist",
        "BMW's 360-degree camera and remote parking technology"
      ],
      images: [
        {
          fields: {
            file: {
              url: img11
            }
          }
        },
        {
          fields: {
            file: {
              url: img11
            }
          }
        },
        {
          fields: {
            file: {
              url: img11
            }
          }
        },
        {
          fields: {
            file: {
              url: img11
            }
          }
        }
      ]
    }
  },
  {
    sys: {
      id: "12"
    },
    fields: {
      idd:"12",
      name: "M5 Touring",
      slug: "M5 Touring",
      type: "hatchback",
      price: 60000,
      doors: 4,
      seats: 5,
      diesel: true,
      automatic: true,
      featured: true,
      description:
        "Street art edison bulb gluten-free, tofu try-hard lumbersexual brooklyn tattooed pickled chambray. Actually humblebrag next level, deep v art party wolf tofu direct trade readymade sustainable hell of banjo. Organic authentic subway tile cliche palo santo, street art XOXO dreamcatcher retro sriracha portland air plant kitsch stumptown. Austin small batch squid gastropub. Pabst pug tumblr gochujang offal retro cloud bread bushwick semiotics before they sold out sartorial literally mlkshk. Vaporware hashtag vice, sartorial before they sold out pok pok health goth trust fund cray.",
      extras: [
        "Four-wheel drive",
        "Wireless CarPlay/Android Auto",
        "Interior Assistant",
        "Adaptive LED headlights",
        "Heated steering wheel",
        "Adaptive cruise control with traffic assist",
        "BMW's 360-degree camera and remote parking technology"
      ],
      images: [
        {
          fields: {
            file: {
              url: img12
            }
          }
        },
        {
          fields: {
            file: {
              url: img12
            }
          }
        },
        {
          fields: {
            file: {
              url: img12
            }
          }
        },
        {
          fields: {
            file: {
              url: img12
            }
          }
        }
      ]
    }
  },
  {
    sys: {
      id: "13"
    },
    fields: {
      idd:"13",
      name: "6-serie Cabrio",
      slug: "6-serie Cabrio",
      type: "hatchback",
      price: 60000,
      doors: 2,
      seats: 4,
      diesel: true,
      automatic: true,
      featured: true,
      description:
        "Street art edison bulb gluten-free, tofu try-hard lumbersexual brooklyn tattooed pickled chambray. Actually humblebrag next level, deep v art party wolf tofu direct trade readymade sustainable hell of banjo. Organic authentic subway tile cliche palo santo, street art XOXO dreamcatcher retro sriracha portland air plant kitsch stumptown. Austin small batch squid gastropub. Pabst pug tumblr gochujang offal retro cloud bread bushwick semiotics before they sold out sartorial literally mlkshk. Vaporware hashtag vice, sartorial before they sold out pok pok health goth trust fund cray.",
      extras: [
        "Four-wheel drive",
        "Wireless CarPlay/Android Auto",
        "Interior Assistant",
        "Adaptive LED headlights",
        "Heated steering wheel",
        "Adaptive cruise control with traffic assist",
        "BMW's 360-degree camera and remote parking technology"
      ],
      images: [
        {
          fields: {
            file: {
              url: img7
            }
          }
        },
        {
          fields: {
            file: {
              url: img7
            }
          }
        },
        {
          fields: {
            file: {
              url: img7
            }
          }
        },
        {
          fields: {
            file: {
              url: img7
            }
          }
        }
      ]
    }
  }
];

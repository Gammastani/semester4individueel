import React from 'react';
import './App.css';

import Home from "./pages/Home";
import Cars from "./pages/Cars";
import SingleCar from "./pages/SingleCar";
import Error from "./pages/Error";
import Register from "./pages/RegisterPage";
import Profile from "./pages/Profile";
import MyCars from "./pages/MyCars";

import {Route, Switch} from "react-router-dom";

import Navbar from "./components/Navbar";
import Login from "./pages/Login";




function App() {
  return (
   <>
       <Navbar/>
       <Switch>
           <Route exact path="/" component={Home}/>
           <Route exact path="/Cars/" component={Cars}/>
           <Route exact path="/Login/" component={Login}/>
           <Route exact path="/Register/" component={Register}/>
           <Route exact path="/Profile/" component={Profile}/>
           <Route exact path="/Cars/:slug" component={SingleCar}/>
           <Route exact path="/MyCars/" component={MyCars}/>
           <Route component={Error}/>
       </Switch>
     </>
  );
}

export default App;

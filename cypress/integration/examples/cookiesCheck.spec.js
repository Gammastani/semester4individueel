

describe('test1',() => {

    beforeEach(()=>{
        cy.visit('http://localhost:3000/login');
    })

    it('has cookie', () =>{
        cy.get('input[name="email"]').type('Alpteking_55@hotmail.com')
        cy.get('input[name="password"]').type('123')
        cy.get('#login').click()
        cy.wait(3000)
        cy.getCookie('id').should('exist');
    })
});
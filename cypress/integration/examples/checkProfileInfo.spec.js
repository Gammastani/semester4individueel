import Chance from 'chance';
const chance = new Chance();

describe('test1',() => {

    beforeEach(()=>{
        cy.visit('http://localhost:3000/login');
    })

    it('has cookie', () =>{
        cy.get('input[name="email"]').type('Alpteking_55@hotmail.com')
        cy.get('input[name="password"]').type('123')
        cy.get('#login').click()
        cy.wait(3000)
        cy.get('#profile').click()
        cy.contains("Alptekin");
        cy.contains("Elver");
        cy.contains("alpteking_55@hotmail.com");
    })
});
